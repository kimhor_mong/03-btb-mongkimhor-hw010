import api from '../api/api';

export const fetchCategory = async () => {
	let response = await api.get('category');
	return response.data.data;
};

export const deleteCategory = async (id) => {
	let response = await api.delete('category/' + id);
	return response.data.message;
};

export const postCategory = async (category) => {
	let response = await api.post('category', category);
	return response.data;
};

export const updateCategoryById = async (id, newCategory) => {
	let response = await api.put('category/' + id, newCategory);
	return response.data.message;
};
