import api from '../api/api';

export const fetchAuthor = async () => {
	let response = await api.get('author');
	return response.data.data;
};

export const deleteAuthor = async (id) => {
	let response = await api.delete('author/' + id);
	return response.data.message;
};

export const postAuthor = async (author) => {
	let response = await api.post('author', author);
	return response.data;
};

export const updateAuthorById = async (id, updatedAuthor) => {
	let response = await api.put('author/' + id, updatedAuthor);
	return response.data.message;
};

export const uploadImage = async (file) => {
	let formData = new FormData();
	formData.append('image', file);

	let response = await api.post('images', formData);
	return response.data.url;
};
