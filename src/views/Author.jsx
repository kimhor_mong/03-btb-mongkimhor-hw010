import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form } from 'react-bootstrap';
import { fetchAuthor, postAuthor, deleteAuthor, uploadImage, updateAuthorById } from '../services/author_service';

function Author() {
	let [authors, setAuthors] = useState([]);

	const [authorName, setAuthorName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');

	useEffect(() => {
		const fetch = async () => {
			let authors = await fetchAuthor();
			setAuthors(authors);
		};
		fetch();
	}, []);

	let onAddOrUpdate = async (e) => {
		e.preventDefault();

		let newAuthor = {
			name: authorName,
			email,
		};

		if (imageFile) {
			let url = await uploadImage(imageFile);
			newAuthor.image = url;
		} else {
			newAuthor.image = imageURL;
		}

		if (selectedId) {
			// update author
			updateAuthorById(selectedId, newAuthor)
				.then((message) => {
					alert(message);
					let newAuthors = authors.map((author) => {
						if (author._id === selectedId) {
							author.name = newAuthor.name;
							author.email = newAuthor.email;
							author.image = newAuthor.image;
						}
						return author;
					});
					setAuthors(newAuthors);
					resetForm();
				})
				.catch((err) => console.log(err.message));
		} else {
			// insert new author
			postAuthor(newAuthor)
				.then((data) => {
					alert(data.message);
					resetForm();
					setAuthors([data.data, ...authors]);
				})
				.catch((err) => console.log(err.message));
		}
	};

	const onDelete = (id) => {
		// if selected author was deleted, we reset the form
		if (id === selectedId) resetForm();

		deleteAuthor(id)
			.then((message) => {
				let remainedAuthors = authors.filter((author) => author._id !== id);
				setAuthors(remainedAuthors);
				alert(message);
			})
			.catch((error) => console.log(error.message));
	};

	let resetForm = () => {
		setAuthorName('');
		setEmail('');
		setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
		setImageFile(null);
		setSelectedId('');
	};

	return (
		<Container>
			<h1 className='my-3'>Author</h1>

			<Row>
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>Author Name</Form.Label>
							<Form.Control
								type='text'
								placeholder='Author Name'
								value={authorName}
								onChange={(e) => setAuthorName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group controlId='email'>
							<Form.Label>Email</Form.Label>
							<Form.Control
								type='text'
								placeholder='Email'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Button variant='primary' onClick={onAddOrUpdate}>
							{selectedId ? 'Update' : 'Add'}
						</Button>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>

			<Table striped bordered hover>
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{authors.map((author, index) => (
						<tr key={author._id}>
							<td>{index + 1}</td>
							<td>{author.name}</td>
							<td>{author.email}</td>
							<td>
								<img style={{ objectFit: 'cover', height: '80px' }} src={author.image} alt='' />
							</td>
							<td>
								<Button
									size='sm'
									variant='warning'
									onClick={() => {
										setSelectedId(author._id);
										setAuthorName(author.name);
										setEmail(author.email);
										setImageURL(author.image);
									}}
								>
									Edit
								</Button>{' '}
								<Button size='sm' variant='danger' onClick={() => onDelete(author._id)}>
									Delete
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}

export default Author;
