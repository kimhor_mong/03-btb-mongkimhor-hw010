import React, { useEffect, useState } from 'react';
import { Container, Form, Table, Button } from 'react-bootstrap';
import { fetchCategory, deleteCategory, postCategory, updateCategoryById } from '../services/category_service';

const Category = () => {
	let [category, setCategory] = useState([]);
	let [newCategory, setNewCategory] = useState('');
	let [selectedCategory, setSelectedCategory] = useState({});

	useEffect(() => {
		fetchCategory().then((response) => {
			setCategory(response);
		});
	}, []);

	const onDelete = (id) => {
		deleteCategory(id).then((message) => alert(message));
		let remainedCategory = [...category];
		remainedCategory = remainedCategory.filter((cate) => cate._id !== id);
		setCategory(remainedCategory);
	};

	const onAddOrUpdate = () => {
		if (selectedCategory._id) {
			updateCategoryById(selectedCategory._id, { name: newCategory })
				.then((message) => {
					alert(message);
					setSelectedCategory({});
					setNewCategory('');
					let allCategory = [...category];
					allCategory = allCategory.map((cate) => {
						if (cate._id === selectedCategory._id) {
							cate['name'] = newCategory;
						}
						return cate;
					});
					setCategory(allCategory);
				})
				.catch((error) => console.log(error));
		} else {
			postCategory({ name: newCategory })
				.then((data) => {
					alert(data.message);
					setCategory([data.data, ...category]);
					setNewCategory('');
				})
				.catch((err) => alert(err.message));
		}
	};

	return (
		<Container>
			<h1 className='my-3'>Category</h1>
			<Form>
				<Form.Group controlId='category'>
					<Form.Label>Category Name</Form.Label>
					<Form.Control
						type='text'
						value={newCategory}
						onChange={(e) => setNewCategory(e.target.value)}
						placeholder='Category Name'
					/>
					<br />
					<Button variant='secondary' onClick={onAddOrUpdate}>
						{selectedCategory._id ? 'Update' : 'Add'}
					</Button>
					<Form.Text className='text-muted'></Form.Text>
				</Form.Group>
			</Form>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{category.map((cate, index) => (
						<tr key={cate._id}>
							<td>{index + 1}</td>
							<td>{cate.name}</td>
							<td>
								<Button
									onClick={() => {
										setSelectedCategory(cate);
										setNewCategory(cate.name);
									}}
									size='sm'
									variant='warning'
								>
									Edit
								</Button>{' '}
								<Button
									onClick={() => {
										onDelete(cate._id);
									}}
									size='sm'
									variant='danger'
								>
									Delete
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
};

export default Category;
